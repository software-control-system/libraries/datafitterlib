from conan import ConanFile

class datafitterlibRecipe(ConanFile):
    name = "datafitterlib"
    version = "2.1.1"
    package_type = "library"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Library"
    
    license = "GPL-2"
    author = "Majid Ounsy", "Berthault"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/libraries/datafitterlib"
    description = "Interpolator library"
    topics = ("utility", "control-system")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    exports_sources = "CMakeLists.txt", "src/**", "include/**"

    def requirements(self):
        self.requires("gsl/1.11@soleil/stable")
        self.requires("yat/[>=1.0]@soleil/stable")
