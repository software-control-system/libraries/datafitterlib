#include <iostream>

#include <FittingFunction.h>

int main()
{
    std::cout << DataFitterVersion_ns::get_version() << '\n';
    return 0;
}
