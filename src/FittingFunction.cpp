#include "FittingFunction.h"

//debut TANGODEVIC-1295
#include <yat/Version.h>
//fin TANGODEVIC-1295


//debut TANGODEVIC-1295

namespace DataFitterVersion_ns
{
//-----------------------------------------------------------------
// get_version
//-----------------------------------------------------------------
const char* get_version()
{
    
    return YAT_XSTR(PROJECT_VERSION);
}
//-----------------------------------------------------------------
// get_name
//-----------------------------------------------------------------
const char* get_name()
{
    return YAT_XSTR(PROJECT_NAME);
}
} // end namespace
//fin TANGODEVIC-1295

FittingResults::FittingResults() :
    qualityFactor(0.), sigma(0.), fwhm(0.), standardDeviation(0.),
    xLow(0.), xHigh(0.), iterations(0) {

}

Solver::Solver(bool scaled, size_t n, size_t p) {
    const gsl_multifit_fdfsolver_type* t = scaled ? gsl_multifit_fdfsolver_lmsder : gsl_multifit_fdfsolver_lmder;
    m_solver = gsl_multifit_fdfsolver_alloc(t, n, p);
}

Solver::~Solver() {
    gsl_multifit_fdfsolver_free(m_solver);
}

gsl_multifit_fdfsolver* Solver::solver() {
    return m_solver;
}

void Solver::dumpState(size_t i) {
    std::cout << "iter: " << i << "x : ";
    dumpVector(m_solver->x, m_solver->x->size);
    std::cout << " |f(x)|=" << gsl_blas_dnrm2(m_solver->f) << std::endl;
}

void dumpVector(const gsl_vector* data, size_t n) {
    std::cout << "dumpVector size of data?? "<<n;
    std::cout << "(";
    if (n)
        std::cout << gsl_vector_get(data, 0);
    for (size_t i=1 ; i < n ; i++)
        std::cout << ", " << gsl_vector_get(data, i);
    std::cout << ")";
}

///debut TANGODEVIC-235


void gaussianEstimateWithFunctionBackground(const FittingData& data, const FittingConfiguration& config, bool estimate_background, gsl_vector* out) {

    size_t n = data.n;
    size_t imin, imax;
    gsl_vector_minmax_index(data.y, &imin, &imax);
    if (config.isReversed)
        std::swap(imin, imax);

    double ymax = gsl_vector_get(data.y, imax);
    // position
    double position = gsl_vector_get(data.x, imax);
    // background
    double background = estimate_background ? gsl_vector_get(data.y, imin) : 0.;
    // height
    double height = ymax - background;
    // width
    size_t i = imax;
    double half = .5 * height + background;

    int sgn = sign_cmp(gsl_vector_get(data.y, i), half);

    if (2*imax < n) { // imax < n/2 (more points available to the right)
        while(i < n-1 && sign_cmp(gsl_vector_get(data.y, i), half) == sgn)
            ++i;
    } else {
        while(i > 0 && sign_cmp(gsl_vector_get(data.y, i), half) == sgn)
            --i;
    }

    double width;
    if (i != imax) {
        width = ::fabs(gsl_vector_get(data.x, i) - position);
    } else { //- try an arbitrary value that should be not so far from reality
        width = n / 6;
    }

    gsl_vector_set(out, 0, position);
    gsl_vector_set(out, 1, width);
    gsl_vector_set(out, 2, height);
    //estimate a and b of background = a*x+b
    //search (x1,y1) and (x2,y2) for calculate background
	  double a; // a of the background = a*x+b
	  double b; // b of the background = a*x+b
	  double double_fwhm = 2.35* width*2; //calcul FWHM * 2 for estimates x1 and x2 of two points M1(x1,y1) and M2(x2,y2) of the background = a*x+b
	  
	  double estimate_x2 = position - double_fwhm; // estimate x2 of M2(x2,y2)
	  double estimate_x1 = position + double_fwhm; // estimate x1 of M1(x1,y1)
	  double eps_x1      = position; // initial min between "estimate x1" and values of the vector of the raw X values
	  double eps_x2      = position; // initial min between "estimate x2" and values of the vector of the raw X values
	  size_t index_x1; //index of the exactes values (x1,y1) of M1 point 
	  size_t index_x2; //index of the exactes values (x2,y2) of M2 point
	  //search exacte index of the x2 value of M2 point :
	  //if abs(v[j] - estimate_x2) > abs(eps_x2) then index of the exacte x2 is found in the vector X
	  //sinon continue
	  size_t j = imax;
	  while(j > 0 && ::fabs(gsl_vector_get(data.x, j) - estimate_x2) < ::fabs(eps_x2))
	  {
		  eps_x2 = fabs(gsl_vector_get(data.x, j) - estimate_x2);
		   --j;
		  }
	  index_x2 = j+1;
	  
	  //search exacte index of the x1 value of M1 point :
	  //if abs(v[j] - estimate_x1) > abs(eps_x1) then index of the exacte x1 is found in the vector X
	  //sinon continue
	  j = imax;
	  while(j < n-1 && ::fabs(gsl_vector_get(data.x, j) - estimate_x1) < ::fabs(eps_x1))
			{
	        eps_x1 = fabs(gsl_vector_get(data.x, j) - estimate_x1);
            ++j;
		    }
	        
	 index_x1 = j-1;
// calcul of a background : a=(y1-y2)/(x1-x2)
     a = (gsl_vector_get(data.y, index_x1) - gsl_vector_get(data.y, index_x2))/(gsl_vector_get(data.x, index_x1) - gsl_vector_get(data.x, index_x2));
//calcul of b background : b = y1 - a*x1
     b = gsl_vector_get(data.y, index_x1) - a*gsl_vector_get(data.x, index_x1);
//set a and b values
     gsl_vector_set(out, 3, a);
     gsl_vector_set(out, 4, b);
     
}

///fin  TANGODEVIC-235

void gaussianEstimate(const FittingData& data, const FittingConfiguration& config, bool estimate_background, gsl_vector* out) {

    size_t n = data.n;
    size_t imin, imax;
    gsl_vector_minmax_index(data.y, &imin, &imax);
    if (config.isReversed)
        std::swap(imin, imax);

    double ymax = gsl_vector_get(data.y, imax);
    // position
    double position = gsl_vector_get(data.x, imax);
    // background
    double background = estimate_background ? gsl_vector_get(data.y, imin) : 0.;
    // height
    double height = ymax - background;
    // width
    size_t i = imax;
    double half = .5 * height + background;

    int sgn = sign_cmp(gsl_vector_get(data.y, i), half);

    if (2*imax < n) { // imax < n/2 (more points available to the right)
        while(i < n-1 && sign_cmp(gsl_vector_get(data.y, i), half) == sgn)
            ++i;
    } else {
        while(i > 0 && sign_cmp(gsl_vector_get(data.y, i), half) == sgn)
            --i;
    }

    double width;
    if (i != imax) {
        width = ::fabs(gsl_vector_get(data.x, i) - position);
    } else { //- try an arbitrary value that should be not so far from reality
        width = n / 6;
    }

    gsl_vector_set(out, 0, position);
    gsl_vector_set(out, 1, width);
    gsl_vector_set(out, 2, height);
    if (estimate_background)
    { 
        gsl_vector_set(out, 3, background);
     }
}

size_t getIndexValue(double target, size_t n, const gsl_vector* data) {
	size_t imax = gsl_vector_max_index(data);
	size_t ilow = imax;
	size_t ihigh = imax;
	size_t i = imax;
	if (2 * imax < n) {
		while (i < n - 1 && gsl_vector_get(data, i) > target) {
			++i;
			ilow = i - 1;
			ihigh = i;
		}
	}
	else {
		while (i > 0 && gsl_vector_get(data, i) > target) {
			--i;
			ilow = i;
			ihigh = i + 1;
		}
	}
	double dlow = ::fabs(gsl_vector_get(data, ilow) - target);
	double dhigh = ::fabs(gsl_vector_get(data, ihigh) - target);
	return dlow < dhigh ? ilow : ihigh;
}

void sigmoidEstimate(const FittingData& data, const FittingConfiguration& config, bool estimate_background, gsl_vector* out) {
	size_t n = data.n;
	size_t imin, imax;
	gsl_vector_minmax_index(data.y, &imin, &imax);
	if (config.isReversed)
		std::swap(imin, imax);

	double ymax = gsl_vector_get(data.y, imax);
	// background
	double background = estimate_background ? gsl_vector_get(data.y, imin) : 0.;
	// height
	double height = ymax - background;
	// position
	double half = .5 * height + background;

	size_t ipos = 0;
	int sgn = sign_cmp(gsl_vector_get(data.y, ipos), half);
	while (ipos < n && sign_cmp(gsl_vector_get(data.y, ipos), half) == sgn)
		++ipos;
	if (ipos == n)
		ipos = n / 2; //- try an arbitrary value that should be not so far from reality
	double position = gsl_vector_get(data.x, ipos);
	// sigma
	double q75 = .75*height + background;
	double q25 = .25*height + background;
	size_t iq75 = getIndexValue(q75, n, data.y);
	size_t iq25 = getIndexValue(q25, n, data.y);
	double xq75 = gsl_vector_get(data.x, iq75);
	double xq25 = gsl_vector_get(data.x, iq25);
	double sigma = .5 * ::fabs(xq75 - xq25);

	gsl_vector_set(out, 0, position);
	gsl_vector_set(out, 1, sigma);
	gsl_vector_set(out, 2, height);
	if (estimate_background)
		gsl_vector_set(out, 3, background);
}

double FittingFunction::computeMean(gsl_vector* vVector) {
    double sum = 0.0;
    for (size_t i=0 ; i < vVector->size ; i++)
        sum += gsl_vector_get(vVector, i);
    return sum / vVector->size;
}

double FittingFunction::computeVariance(gsl_vector* vVector) {
    double mean = computeMean(vVector);
    double sum = 0.0;
    for (size_t i=0 ; i < vVector->size ; i++)
        sum += square(gsl_vector_get(vVector, i) - mean);
    return sum; // / dNbData;
}

double FittingFunction::computeResidual(gsl_vector* vVectorExp,gsl_vector* vVectorPred) {
    double sum = 0.0;
    for (size_t i=0 ; i < vVectorExp->size ; i++)
        sum += square(gsl_vector_get(vVectorExp,i) - gsl_vector_get(vVectorPred,i));
    return sum; // / dNbData;
}

double FittingFunction::computeDeterminationCoefficient(gsl_vector* vExperimentalDataY,gsl_vector* vFittedDataY) {
    double dDeviation = computeVariance(vExperimentalDataY);
    double dResidual = computeResidual(vExperimentalDataY,vFittedDataY);
    return 1 - dResidual/dDeviation;
}

double FittingFunction::computeStandardDeviation(gsl_vector* vVector) {
    size_t n = vVector->size;
    double sum = 0.;
    double ssum = 0.;
    for (size_t i=0 ; i < n ; i++) {
        double v = gsl_vector_get(vVector, i);
        sum += v;
        ssum += square(v);
    }
    double variance = ssum/n - square(sum/n);
    return sqrt(variance);
}

FittingFunction::FittingFunction(size_t paramsCount):
        mParamsCount(paramsCount),
        mFunction(NULL),
        mFunctionDerivate(NULL),
        mFunctionAndDerivate(NULL) {
    mParameters = gsl_vector_alloc(mParamsCount);
    mParametersErrors = gsl_vector_alloc(mParamsCount);
    mInitialGuess = gsl_vector_alloc(mParamsCount);
}

FittingFunction::~FittingFunction() {
    std::cout << "destroying fitting function " << mName << " ..." << std::endl;
    gsl_vector_free(mParameters);
    gsl_vector_free(mParametersErrors);
    gsl_vector_free(mInitialGuess);
}

void FittingFunction::initializeInitialsParameters(const FittingData& data, const FittingConfiguration& config, const gsl_vector *guess, bool estimate) {

    if (estimate)
    {
        estimateInitialGuess(data, config);
     }   
    else { // gsl_vector_memcpy ?
        for(size_t i=0 ; i < mParamsCount ; ++i)
         
            gsl_vector_set(mInitialGuess, i, gsl_vector_get(guess, i));
    }
}

int FittingFunction::doFit(FittingData& data, const FittingConfiguration& config, const gsl_vector *guess, bool estimate) {  
    return genericFit(data, config, guess, estimate);
}

int FittingFunction::genericFit(FittingData& data, const FittingConfiguration& config, const gsl_vector *guess, bool estimate) {
    //The size of the 3 gsl_vector must be the same. Not checked here !!!

    initializeInitialsParameters(data, config, guess, estimate);

    std::cout << "\n---------------------------------------------" << std::endl;
    std::cout << "Fit Configuration" << std::endl;
    std::cout << "* Jacobian Data " << (config.scaled ? "Scaled" : "Not Scaled") << std::endl;

    switch(config.searchStoppingMethod) {
    case 1: std::cout << "* Test Delta Search Stopping Function Used" << std::endl; break;
    case 2: std::cout << "* Test Gradient Search Stopping Function Used" << std::endl; break;
    }
    Solver solver(config.scaled, data.n, mParamsCount);
    gsl_multifit_fdfsolver *s = solver.solver();

    gsl_multifit_function_fdf f;
    f.f = mFunction;
    f.df = mFunctionDerivate;
    f.fdf = mFunctionAndDerivate;
    f.n = data.n;
    f.p = mParamsCount;
    f.params = &data;

    std::cout << "\n----------------------------------------------" << std::endl;
    std::cout << "Initials Guess For The Fit " << std::endl;
    dumpVector(mInitialGuess, mParamsCount);
    gsl_multifit_fdfsolver_set(s, &f, mInitialGuess);
    

    size_t iter = 0;
    int status;

    std::cout << "\n---------------------------------------------" << std::endl;
    std::cout << "Iterations" << std::endl;
    solver.dumpState(iter);

    do {
        iter++;
        status = gsl_multifit_fdfsolver_iterate(s);

        std::cout << "status = " << gsl_strerror(status) << std::endl;
        solver.dumpState(iter);

        if (status)
            break;

        switch(config.searchStoppingMethod) {
        case 1:
            status = gsl_multifit_test_delta(s->dx, s->x, 0., config.epsilon);
            break;
        case 2:
            gsl_vector* gradient = gsl_vector_alloc(mParamsCount);
            gsl_multifit_gradient(s->J, s->f, gradient);
            status = gsl_multifit_test_gradient(gradient, config.epsilon);
            gsl_vector_free(gradient);
            break;
        }
    } while (status == GSL_CONTINUE && iter < config.maxIterations);

    std::cout << "status = " << gsl_strerror(status) << std::endl;

    gsl_matrix *covar = gsl_matrix_alloc(mParamsCount, mParamsCount);
    gsl_multifit_covar(s->J, 0., covar);
    for(size_t i=0 ; i < mParamsCount ; i++) {
        gsl_vector_set(mParameters, i, gsl_vector_get(s->x, i));
        gsl_vector_set(mParametersErrors, i, sqrt(gsl_matrix_get(covar,i,i)));
    }
    gsl_matrix_free(covar);

    mFitResults.iterations = iter;
    mFitResults.qualityFactor = square(gsl_blas_dnrm2(s->f))/ (data.n - mParamsCount);
    computeResults();

    return status;

}

void FittingFunction::computeResults() {

}

double FittingFunction::computeObservedFStatistic(gsl_vector* vExperimentalDataY,gsl_vector* vFittedDataY) {
    double n = vExperimentalDataY->size;
    double p = mParamsCount;
    double deviation = computeVariance(vExperimentalDataY);
    double residual = computeResidual(vExperimentalDataY,vFittedDataY);
    double msr = (deviation - residual)/p;
    double mse = residual/(n-1-p);
    return msr/mse;
}
