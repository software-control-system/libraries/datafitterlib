#include "FittingFunctionSigmoid.h"

FittingFunctionSigmoid::FittingFunctionSigmoid() {
    mName = "Sigmoid";
    mEquation = "f(x) = Height / [ 1 + exp[ - (x - Position) / Width] ]";
}

void FittingFunctionSigmoid::computeResults() {
    mFitResults.sigma = 1.0/ (sqrt(2*M_PI) * gsl_vector_get(mParameters, 2));
		// FWHM = 2* sigma * (-ln(4*(1-SQRT(1/2))-1))
	double dTmp = -2.0 * log(4.0*(1-M_SQRT1_2) - 1.0);
	mFitResults.fwhm = dTmp * gsl_vector_get(mParameters, 1);
	
    SigmoidFunction sf(mParameters);
    double x0 = sf.pos;
    double ylow = 0.; // background
    double yhigh = ylow + sf.height;
    double v = sf(x0);
    double dv = sf.derivate(x0);
    mFitResults.xLow = (ylow - v)/dv + x0;
    mFitResults.xHigh = (yhigh - v)/dv + x0;
}

void FittingFunctionSigmoid::estimateInitialGuess(const FittingData& data, const FittingConfiguration& config) {
    sigmoidEstimate(data, config, false, mInitialGuess);
}
