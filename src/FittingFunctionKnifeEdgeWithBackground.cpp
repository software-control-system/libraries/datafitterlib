#include "FittingFunctionKnifeEdgeWithBackground.h"

FittingFunctionKnifeEdgeWithBackground::FittingFunctionKnifeEdgeWithBackground()
{
	mName = "KnifeEdge With Background";
	mEquation = "f(x) = Height/2 * { 1 + erf[(x - Mu)/(sqrt(2) * Sigma)]} + Background";
}

void FittingFunctionKnifeEdgeWithBackground::computeResults()
{
	// FWHM = 2 * sqrt (2 * ln(2)) * sigma
	mFitResults.fwhm = 2.*sqrt(2.*log(2.)) * gsl_vector_get(mParameters, 1);
}

void FittingFunctionKnifeEdgeWithBackground::estimateInitialGuess(const FittingData& data, const FittingConfiguration& config)
{
	// Parameters initial values could be estimated as a sigmo�d.
	sigmoidEstimate(data, config, true, mInitialGuess);
}
