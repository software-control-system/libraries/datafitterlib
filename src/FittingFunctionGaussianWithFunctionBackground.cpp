#include "FittingFunctionGaussianWithFunctionBackground.h"
//debut TANGODEVIC-235
FittingFunctionGaussianWithFunctionBackground::FittingFunctionGaussianWithFunctionBackground() {
    mName = "Gaussian With Function Background";
    mEquation = "f(x) = Height * exp[ - (x - Position)" SUPERSCRIPT_TWO "/ (2 * Width" SUPERSCRIPT_TWO ")] + a*x + b";
}

void FittingFunctionGaussianWithFunctionBackground::computeResults() {
    mFitResults.fwhm = 2.*sqrt(2.*log(2.)) * gsl_vector_get(mParameters, 1);
}

void FittingFunctionGaussianWithFunctionBackground::estimateInitialGuess(const FittingData& data, const FittingConfiguration& config) {
    gaussianEstimateWithFunctionBackground(data, config, true, mInitialGuess);
}

//fin TANGODEVIC-235
