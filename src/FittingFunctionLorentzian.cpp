#include "FittingFunctionLorentzian.h"

FittingFunctionLorentzian::FittingFunctionLorentzian() {
    mName = "Lorentzian";
    mEquation = "f(x) = Height / [1 + (x - Position)" SUPERSCRIPT_TWO " / Width" SUPERSCRIPT_TWO "]";
}

void FittingFunctionLorentzian::computeResults() {
    mFitResults.fwhm = 2. * gsl_vector_get(mParameters, 1);
}

void FittingFunctionLorentzian::estimateInitialGuess(const FittingData& data, const FittingConfiguration& config) {
    gaussianEstimate(data, config, false, mInitialGuess);
}
