#include "FittingFunctionGaussianWithBackground.h"

FittingFunctionGaussianWithBackground::FittingFunctionGaussianWithBackground() {
    mName = "Gaussian With Background";
    mEquation = "f(x) = Height * exp[ - (x - Position)" SUPERSCRIPT_TWO "/ (2 * Width" SUPERSCRIPT_TWO ")] + Background";
}

void FittingFunctionGaussianWithBackground::computeResults() {
    mFitResults.fwhm = 2.*sqrt(2.*log(2.)) * gsl_vector_get(mParameters, 1);
}

void FittingFunctionGaussianWithBackground::estimateInitialGuess(const FittingData& data, const FittingConfiguration& config) {
    gaussianEstimate(data, config, true, mInitialGuess);
}
