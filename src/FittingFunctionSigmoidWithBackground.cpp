#include "FittingFunctionSigmoidWithBackground.h"
#include "FittingFunctionSigmoid.h"

FittingFunctionSigmoidWithBackground::FittingFunctionSigmoidWithBackground() {
    mName = "Sigmoid With Background";
    mEquation = "f(x) = Height / [ 1 + exp[ - (x - Position) / Width ] ] + Background";
}

void FittingFunctionSigmoidWithBackground::computeResults() {
	// FWHM = 2* sigma * (-ln(4*(1-SQRT(1/2))-1))
	double dTmp = -2.0 * log(4.0*(1-M_SQRT1_2) - 1.0);
	mFitResults.fwhm = dTmp * gsl_vector_get(mParameters, 1);
	
    SigmoidbFunction sf(mParameters);
    double x0 = sf.pos;
    double ylow = sf.background;
    double yhigh = ylow + sf.height;
    double v = sf(x0);
    double dv = sf.derivate(x0);
    mFitResults.xLow = (ylow - v)/dv + x0;
    mFitResults.xHigh = (yhigh - v)/dv + x0;
	
}

void FittingFunctionSigmoidWithBackground::estimateInitialGuess(const FittingData& data, const FittingConfiguration& config) {
    sigmoidEstimate(data, config, true, mInitialGuess);
}
