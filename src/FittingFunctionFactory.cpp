#include "FittingFunctionFactory.h"

#include "FittingFunctionGaussian.h"
#include "FittingFunctionGaussianWithBackground.h"
#include "FittingFunctionLorentzian.h"
#include "FittingFunctionLorentzianWithBackground.h"
#include "FittingFunctionMeanWithThreshold.h"
#include "FittingFunctionSigmoid.h"
#include "FittingFunctionSigmoidWithBackground.h"
//debut TANGODEVIC-235
#include "FittingFunctionGaussianWithFunctionBackground.h"
#include "FittingFunctionLorentzianWithFunctionBackground.h"
//fin TANGODEVIC-235
#include "FittingFunctionKnifeEdgeWithBackground.h" // Ajout pour [TANGODEVIC-1843]

std::set<std::string> FittingFunctionFactory::knownFitters() {
    std::set<std::string> result;
    result.insert("gaussian");
    result.insert("gaussianb");
    result.insert("lorentzian");
    result.insert("lorentzianb");
    result.insert("sigmoid");
    result.insert("sigmoidb");
    result.insert("meanwiththreshold");
//debut TANGODEVIC-235
    result.insert("gaussianbf");
    result.insert("lorentzianbf");
//fin TANGODEVIC-235
	result.insert("knifeedgeb"); // Ajout pour [TANGODEVIC-1843]
	return result;
}

FittingFunction* FittingFunctionFactory::newFitter(const std::string& name) {
    if (name == "gaussian")
        return new FittingFunctionGaussian;
    if (name == "gaussianb")
        return new FittingFunctionGaussianWithBackground;
    if (name == "lorentzian")
        return new FittingFunctionLorentzian;
    if (name == "lorentzianb")
        return new FittingFunctionLorentzianWithBackground;
    if (name == "sigmoid")
        return new FittingFunctionSigmoid;
    if (name == "sigmoidb")
        return new FittingFunctionSigmoidWithBackground;
    if (name == "meanwiththreshold")
        return new FittingFunctionMeanWithThreshold;
//debut TANGODEVIC-235
    if(name == "lorentzianbf")
		return new FittingFunctionLorentzianWithFunctionBackground;
    if (name == "gaussianbf")
        return new FittingFunctionGaussianWithFunctionBackground;
//fin TANGODEVIC-235
	if (name == "knifeedgeb")
		return new FittingFunctionKnifeEdgeWithBackground; // Ajout pour [TANGODEVIC-1843]

    return NULL;
}
