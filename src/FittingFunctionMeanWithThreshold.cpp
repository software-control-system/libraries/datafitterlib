#include <vector>
#include <gsl/gsl_interp.h>
#include "FittingFunctionMeanWithThreshold.h"

FittingFunctionMeanWithThreshold::FittingFunctionMeanWithThreshold() : FittingFunction(4) {
    mName = "Mean With Threshold";
    mEquation = "f(x) = Height * exp[ - (x - Position)" SUPERSCRIPT_TWO " / (2 * Width" SUPERSCRIPT_TWO ")] + Background";
}

int FittingFunctionMeanWithThreshold::doFit(FittingData& data, const FittingConfiguration& config, const gsl_vector *guess, bool estimate)
{
    double x_0;
    double x_1;
    double y_min;
    double y_max;
    double position;
    double background;
    double height;
    double width;
    double factor;
    double threshold;

    initializeInitialsParameters (data, config, guess, estimate);

    std::cout << "\n---------------------------------------------" << std::endl;
    std::cout << "Initials Parameters For The Fit" << std::endl;
    std::cout << "\n----------------------------------------------" << std::endl;
    std::cout << "Initials Values For The Fit " << std::endl;
    dumpVector(mInitialGuess, 4);

    /*
     * find the min and max of x (they should be unordered)
     * use thoses values in cas of wrong threshold to do the
     * computation
     */
    gsl_vector_minmax (data.x, &x_0, &x_1);

    // find the min and max indexes and values of y
    gsl_vector_minmax (data.y, &y_min, &y_max);

    // first check if the threshold is in between the min max.
    factor = gsl_vector_get (guess, 3);
    threshold = y_min + (y_max - y_min) * factor / 100.;
    if (threshold <= y_max && threshold >= y_min){
        size_t len;
        size_t i;
        std::vector<size_t> indices;

        /*
         * let find all intersections from the first point
         * and keep only the first one and the last one.
         * get the four indexes of these points.
         */
        len = data.x->size - 1; /// TODO data.n ?
        for(i=0; i<len; ++i)
            if( (threshold - gsl_vector_get (data.y, i)) * (threshold - gsl_vector_get (data.y, i+1)) < 0)
                indices.push_back(i);

        if(indices.size() > 0){
            x_0 = this->find_intersection (data.x, data.y, indices.front(), threshold);
            // if more than one intersection use the last one
            if(indices.size() >= 2)
                x_1 = this->find_intersection (data.x, data.y, indices.back(), threshold);

            std::cout << "i0 " << indices.front() << " i1 " << indices.back() << std::endl;
        }
    }

    std::cout << "threshold " << threshold << " x_0 " << x_0 << " x_1 " << x_1 << std::endl;

    width = fabs(x_0 - x_1);
    position = (x_0 + x_1) / 2.;
    height = y_max - y_min;
    background = factor;

    // fill the initial guess vector
    gsl_vector_set (mInitialGuess, 0, position);
    gsl_vector_set (mInitialGuess, 1, width);
    gsl_vector_set (mInitialGuess, 2, height);
    gsl_vector_set (mInitialGuess, 3, background);

    for(size_t i=0 ; i < 4 ; i++) {
        gsl_vector_set(mParameters, i, gsl_vector_get(mInitialGuess, i));
        gsl_vector_set(mParametersErrors, i, 0.);
    }

    mFitResults.qualityFactor = pow(1.0, 2.0)/ (data.n - mParamsCount);
    mFitResults.fwhm = 2.*sqrt(2.*log(2.)) * gsl_vector_get(mParameters, 1);

    return GSL_SUCCESS;
}

void FittingFunctionMeanWithThreshold::estimateInitialGuess(const FittingData& data, const FittingConfiguration& config)
{
    // fill the initials parameters values estimation vector for the initialisation
    // [0] = position
    // [1] = width
    // [2] = height
    // [3] = background
    // Thanks to the max index of all the points, we can get the
    // position_value	= x_value at index_max
    // width			= standard deviation
    // height_value		= y_value - min_value (=background)	at index_max
    // background_value = minimal value
    //min_index = gsl_vector_min_index (vExperimentalDataY);
    //max_index = gsl_vector_max_index (vExperimentalDataY);

    // find the min and max indexes and values of y
    size_t max_index;
    size_t min_index;
    gsl_vector_minmax_index (data.y, &min_index, &max_index);
    double max = gsl_vector_get(data.y, max_index);

    double position = gsl_vector_get (data.x, max_index);
    double background = gsl_vector_get(data.y, min_index);;
    double height = max - background;
    double threshold = max - height / 2.;
    double sigma = compute_width(data.x, data.y, threshold, max, max_index);

    // fill the initial guess vector
    gsl_vector_set (mInitialGuess, 0, position);
    gsl_vector_set (mInitialGuess, 1, sigma);
    gsl_vector_set (mInitialGuess, 2, height);
    gsl_vector_set (mInitialGuess, 3, background);
}

double FittingFunctionMeanWithThreshold::compute_width(gsl_vector *x, gsl_vector *y,
        double threshold, double max, size_t index_max)
{
    size_t index;
    size_t index_low;
    size_t index_high;
    size_t len;
    double value;

    // now compute the sigma.
    value  = max;
    index = index_low = index_high = index_max;
    len = x->size;

    // first start from the maximum and go to the lower indexes.
    while(index > 0 && value > threshold)
        value = gsl_vector_get (y, --index);
    index_low = index;

    // now in the other direction
    index = index_max;
    while(index < len && value > threshold)
        value = gsl_vector_get (y, ++index);
    index_high = index;

    return gsl_vector_get (x, index_high) - gsl_vector_get (x, index_low);
}


int FittingFunctionMeanWithThreshold::generateFunctionFit(double dValMinX, double dResolutionX, long lNbPoints,
        gsl_vector* funcX, gsl_vector* funcY)
{

    int i;
    double dPos	   = gsl_vector_get(mParameters, 0);
    double dWidth	   = gsl_vector_get(mParameters, 1);
    double dHeight	   = gsl_vector_get(mParameters, 2);
    double dBackground = gsl_vector_get(mParameters, 3);
    double dXi	   = 0;
    double dXPos	   = 0;
    double dXPos2	   = 0;
    double dWidth2	   = 1;
    double dYi	   = 0;

    for (i=0;i<lNbPoints;i++){
        dXi	= dValMinX+i*dResolutionX;
        dXPos	= dXi - dPos;
        dXPos2	= dXPos * dXPos;
        dWidth2	= dWidth * dWidth;
        dYi	= dBackground + dHeight * exp(-0.5 * dXPos2 / dWidth2);

        gsl_vector_set (funcX, i, dXi);
        gsl_vector_set (funcY, i, dYi);
    }
    return 1;
}


int FittingFunctionMeanWithThreshold::generateFunctionFit(long lNbPoints, gsl_vector *x,
        gsl_vector* funcX, gsl_vector* funcY)
{
    int i;
    double dPos	   = gsl_vector_get(mParameters, 0);
    double dWidth	   = gsl_vector_get(mParameters, 1);
    double dHeight	   = gsl_vector_get(mParameters, 2);
    double dBackground = gsl_vector_get(mParameters, 3);
    double dXi	   = 0;
    double dXPos	   = 0;
    double dXPos2	   = 0;
    double dWidth2	   = 1;
    double dYi	   = 0;

    for (i=0;i<lNbPoints;i++){
        dXi	= gsl_vector_get (x,i);
        dXPos	= dXi - dPos;
        dXPos2	= dXPos * dXPos;
        dWidth2	= dWidth * dWidth;
        dYi	= dBackground + dHeight * exp(-0.5 * dXPos2 / dWidth2);

        gsl_vector_set (funcX, i, dXi);
        gsl_vector_set (funcY, i, dYi);
    }
    return 1;
}

int FittingFunctionMeanWithThreshold::generateDerivativeFunctionFit(double dValMinX, double dResolutionX, long lNbPoints,
        gsl_vector* funcX, gsl_vector* funcY)
{

    int i;
    double dPos    = gsl_vector_get(mParameters, 0);
    double dWidth  = gsl_vector_get(mParameters, 1);
    double dHeight = gsl_vector_get(mParameters, 2);
    double dXi     = 0;
    double dXPos   = 0;
    double dXPos2  = 0;
    double dWidth2 = 1;
    double dYi     = 0;

    for (i=0;i<lNbPoints;i++){
        dXi	= dValMinX+i*dResolutionX;
        dXPos	= dXi - dPos;
        dXPos2	= dXPos * dXPos;
        dWidth2	= dWidth * dWidth;
        dYi	= - dHeight / dWidth2 * dXPos * exp(-0.5 * dXPos2 / dWidth2);

        gsl_vector_set (funcX, i, dXi);
        gsl_vector_set (funcY, i, dYi);
    }
    return 1;
}

int FittingFunctionMeanWithThreshold::generateDerivativeFunctionFit(long lNbPoints, gsl_vector *x, gsl_vector* funcX, gsl_vector* funcY)
{
    int i;
    double dPos    = gsl_vector_get(mParameters, 0);
    double dWidth  = gsl_vector_get(mParameters, 1);
    double dHeight = gsl_vector_get(mParameters, 2);
    double dXi     = 0;
    double dXPos   = 0;
    double dXPos2  = 0;
    double dWidth2 = 1;
    double dYi     = 0;

    for (i=0;i<lNbPoints;i++) {
        dXi	= gsl_vector_get(x,i);
        dXPos	= dXi - dPos;
        dXPos2	= dXPos * dXPos;
        dWidth2	= dWidth * dWidth;
        dYi	= - dHeight / dWidth2 * dXPos * exp(-0.5 * dXPos2 / dWidth2);

        gsl_vector_set (funcX, i, dXi);
        gsl_vector_set (funcY, i, dYi);
    }
    return 1;
}

double FittingFunctionMeanWithThreshold::find_intersection(gsl_vector *x, gsl_vector *y, size_t index, double threshold)
{
    /*
     * now it is time to do a linear interpolation between index and index+1
     */
    gsl_interp_accel *acc = gsl_interp_accel_alloc();
    gsl_interp *interp = gsl_interp_alloc(gsl_interp_linear, 2);
    double x_interp;
    double xx[2]; // will store in fact y[i]
    double yy[2]; // will store in fact x[i]

    xx[0] = gsl_vector_get(y, index);
    yy[0] = gsl_vector_get(x, index);
    xx[1] = gsl_vector_get(y, index+1);
    yy[1] = gsl_vector_get(x, index+1);
    // need to order the xx and yy vector.
    if(xx[0] > xx[1]){
        double tmp;

        tmp = xx[0];
        xx[0] = xx[1];
        xx[1] = tmp;

        tmp = yy[0];
        yy[0] = yy[1];
        yy[1] = tmp;
    }

    gsl_interp_init(interp, xx, yy, 2);
    x_interp = gsl_interp_eval(interp, xx, yy, threshold, acc);

    gsl_interp_accel_free(acc);
    gsl_interp_free(interp);

    return x_interp;
}
