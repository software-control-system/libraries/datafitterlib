#include "FittingFunctionLorentzianWithFunctionBackground.h"
//debut TANGODEVIC-235
FittingFunctionLorentzianWithFunctionBackground::FittingFunctionLorentzianWithFunctionBackground() {
    mName = "Lorentzian With Function Background";
    mEquation = "f(x) = Height / [1 + (x - Position)" SUPERSCRIPT_TWO " / Width" SUPERSCRIPT_TWO "] + a*x + b";
}

void FittingFunctionLorentzianWithFunctionBackground::computeResults() {
    mFitResults.fwhm = 2. * gsl_vector_get(mParameters, 1);
}

void FittingFunctionLorentzianWithFunctionBackground::estimateInitialGuess(const FittingData& data, const FittingConfiguration& config) {
    gaussianEstimateWithFunctionBackground(data, config, true, mInitialGuess);
}
//fin TANGODEVIC-235
