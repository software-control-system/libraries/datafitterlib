#include "FittingFunctionGaussian.h"

FittingFunctionGaussian::FittingFunctionGaussian() {
    mName = "Gaussian";
    mEquation = "f(x) = Height * exp[ - (x - Position)" SUPERSCRIPT_TWO "/ (2 * Width" SUPERSCRIPT_TWO ")]";
}

void FittingFunctionGaussian::computeResults() {
    mFitResults.fwhm = 2.*sqrt(2.*log(2.)) * gsl_vector_get(mParameters, 1);
}

void FittingFunctionGaussian::estimateInitialGuess(const FittingData& data, const FittingConfiguration& config) {
    gaussianEstimate(data, config, false, mInitialGuess);
}
