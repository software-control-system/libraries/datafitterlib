#include "FittingFunctionLorentzianWithBackground.h"

FittingFunctionLorentzianWithBackground::FittingFunctionLorentzianWithBackground() {
    mName = "Lorentzian With Background";
    mEquation = "f(x) = Height / [1 + (x - Position)" SUPERSCRIPT_TWO " / Width" SUPERSCRIPT_TWO "] + Background";
}

void FittingFunctionLorentzianWithBackground::computeResults() {
    mFitResults.fwhm = 2. * gsl_vector_get(mParameters, 1);
}

void FittingFunctionLorentzianWithBackground::estimateInitialGuess(const FittingData& data, const FittingConfiguration& config) {
    gaussianEstimate(data, config, true, mInitialGuess);
}
