#ifndef FITTING_FUNCTION_SIGMOIDB_H
#define FITTING_FUNCTION_SIGMOIDB_H

#include "FittingFunction.h"

struct SigmoidbFunction {

    static const size_t size = 4;

    double pos, width, height, background;

    SigmoidbFunction(const gsl_vector* params) {
        pos = gsl_vector_get(params, 0);
        width = gsl_vector_get(params, 1);
        height = gsl_vector_get(params, 2);
        background = gsl_vector_get(params, 3);
    }

    inline double operator()(double x) const {
        return height / (1. + exp(-(x-pos)/width)) + background;
    }

    inline double derivate(double x) const {
        double rcx = (x-pos)/width;
        double expo = exp(-rcx);
        return  height/width * expo / square(1. + expo);
    }

    inline void partial(double x, double* dy) const {
        double rcx = (x-pos)/width;
        double expo = exp(-rcx);
        double e = 1. + expo;
        double e2 = square(e);
        dy[0] = - height/width * expo / e2;
        dy[1] = - height/width * rcx * expo / e2;
        dy[2] = 1. / e;
        dy[3] = 1.;
    }

};

class FittingFunctionSigmoidWithBackground : public GenericFitter<SigmoidbFunction> {

public:

    FittingFunctionSigmoidWithBackground();
    void estimateInitialGuess(const FittingData& data, const FittingConfiguration& config);
    void computeResults();

};

#endif // FITTING_FUNCTION_SIGMOIDB_H
