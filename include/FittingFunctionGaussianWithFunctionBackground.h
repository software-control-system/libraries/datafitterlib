#ifndef FITTING_FUNCTION_GAUSSIAN_FUNCTIONB_H
#define FITTING_FUNCTION_GAUSSIAN_FUNCTIONB_H

#include "FittingFunction.h"
// debut TANGODEVIC-235
struct GaussianFbFunction {
//init the parameters and derivatives of the gaussian function
//with background = a*x+b
    static const size_t size = 5; //number of the fit parameters

    double pos, width, height, background_a, background_b;

    GaussianFbFunction(const gsl_vector* params) {
        pos = gsl_vector_get(params, 0); //position
        width = gsl_vector_get(params, 1); //width
        height = gsl_vector_get(params, 2);//height
        background_a = gsl_vector_get(params, 3);//background_a
	background_b = gsl_vector_get(params, 4);//background_b
    }

    inline double operator()(double x) const {
//calcul gaussian function with background = a*x+b
//gaussian function is defined by :
        return height * exp(-.5 * square((x-pos)/width)) + background_a *x + background_b;
    }

    inline double derivate(double x) const {
//calcul partial derivative of x of the gaussian function
        double rcx = (x-pos)/width;
//partial derivative of x of gaussian function
        return - height * rcx / width * exp(-.5 * square(rcx)) + background_a;
    }

    inline void partial(double x, double* dy) const {
//calculs partials derivatives of the parameters position, width, 
//height, background = a*x+b of the gaussian function
        double rcx = (x-pos)/width;
        double rcx2 = square(rcx);
        double e = exp(-.5 * rcx2);
//partial derivative of position
        dy[0] = height * rcx/width * e;
//partial derivative of width
        dy[1] = height * rcx2/width * e;
//partial derivative of height
        dy[2] = e;
//partial derivative of background = a*x+b
   // partial derivative of a
        dy[3] = x; 
   // partial derivative of b
	    dy[4] = 1.;
    }

};

class FittingFunctionGaussianWithFunctionBackground : public GenericFitter<GaussianFbFunction> {

public:

    FittingFunctionGaussianWithFunctionBackground();
    void estimateInitialGuess(const FittingData& data, const FittingConfiguration& config);
    void computeResults();

};
// fin TANGODEVIC-235
#endif // FITTING_FUNCTION_GAUSSIAN_FUNCTIONB_H
