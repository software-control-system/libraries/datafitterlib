#ifndef FITTING_FUNCTION_MEAN_WITH_THRESHOLD_H
#define FITTING_FUNCTION_MEAN_WITH_THRESHOLD_H

#include "FittingFunction.h"

class FittingFunctionMeanWithThreshold : public FittingFunction {

public:

    FittingFunctionMeanWithThreshold();

    int doFit(FittingData& data, const FittingConfiguration& config, const gsl_vector *guess, bool estimate);
    void estimateInitialGuess(const FittingData& data, const FittingConfiguration& config);

    int generateFunctionFit(double dValMinX, double dResolutionX, long lNbPoints, gsl_vector* funcX, gsl_vector* funcY);
    int generateFunctionFit(long lNbPoints, gsl_vector *x, gsl_vector* funcX,gsl_vector* funcY);
    int generateDerivativeFunctionFit(double dValMinX,double dResolutionX,long lNbPoints,gsl_vector* funcX,gsl_vector* funcY);
    int generateDerivativeFunctionFit(long lNbPoints, gsl_vector *x, gsl_vector* funcX,gsl_vector* funcY);

private:
    double compute_width(gsl_vector *x, gsl_vector *y, double threshold, double max, size_t index_max);
    double find_intersection(gsl_vector *x, gsl_vector *y, size_t index, double threshold);

};

#endif // FITTING_FUNCTION_MEAN_WITH_THRESHOLD_H
