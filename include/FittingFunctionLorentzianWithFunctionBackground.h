#ifndef FITTING_FUNCTION_LORENTZIAN_FUNCTIONB_H
#define FITTING_FUNCTION_LORENTZIAN_FUNCTIONB_H

#include "FittingFunction.h"
// debut TANGODEVIC-235
struct LorentzianFbFunction {
//init the parameters and derivatives of the lorentzian function
//with background = a*x+b
    static const size_t size = 5; //number of the fit parameters

    double pos, width, height, background_a, background_b;

    LorentzianFbFunction(const gsl_vector* params) {
        pos = gsl_vector_get(params, 0); //position
        width = gsl_vector_get(params, 1); //width
        height = gsl_vector_get(params, 2); //height
        background_a = gsl_vector_get(params, 3); //background_a
        background_b = gsl_vector_get(params, 4);//background_b
    }

    inline double operator()(double x) const {
//calcul lorentzian function with background = a*x+b
//lorentzian function is defined by :
        return height / (1. + square((x-pos)/width)) + background_a *x + background_b;
    }

    inline double derivate(double x) const {
		//calcul partial derivative of x of the lorentzian function
        double rcx = (x-pos)/width;
        double e = 1. + square(rcx);
        return - 2. * height / width * rcx / square(e) + background_a;
    }

    inline void partial(double x, double* dy) const {
//calculs partials derivatives of the parameters position, width, 
//height, background = a*x+b of the lorentzian function
        double rcx = (x-pos)/width;
        double e = 1. + square(rcx);
//partial derivative of position
        dy[0] = 2. * height / width * rcx / square(e);
//partial derivative of width
        dy[1] = 2. * height / width * square(rcx) / square(e);
//partial derivative of height
        dy[2] = 1./e;
//partial derivative of background = a*x+b
   // partial derivative of a
        dy[3] = x; 
   // partial derivative of b
	    dy[4] = 1.;
    }

};

class FittingFunctionLorentzianWithFunctionBackground : public GenericFitter<LorentzianFbFunction> {

public:

    FittingFunctionLorentzianWithFunctionBackground();
    void estimateInitialGuess(const FittingData& data, const FittingConfiguration& config);
    void computeResults();

};
//fin TANGODEVIC-235
#endif // FITTING_LORENTZIAN_FUNCTIONB_H
