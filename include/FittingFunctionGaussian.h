#ifndef FITTING_FUNCTION_GAUSSIAN_H
#define FITTING_FUNCTION_GAUSSIAN_H

#include "FittingFunction.h"

struct GaussianFunction {

    static const size_t size = 3;

    double pos, width, height;

    GaussianFunction(const gsl_vector* params) {
        pos = gsl_vector_get(params, 0);
        width = gsl_vector_get(params, 1);
        height = gsl_vector_get(params, 2);
    }

    inline double operator()(double x) const {
        return height * exp(-.5 * square((x-pos)/width));
    }

    inline double derivate(double x) const {
        double rcx = (x-pos)/width;
        return - height * rcx / width * exp(-.5 * square(rcx));
    }

    inline void partial(double x, double* dy) const {
        double rcx = (x-pos)/width;
        double rcx2 = square(rcx);
        double e = exp(-.5 * rcx2);
        dy[0] = height/width * rcx * e;
        dy[1] = height/width * rcx2 * e;
        dy[2] = e;
    }

};

class FittingFunctionGaussian : public GenericFitter<GaussianFunction> {

public:

    FittingFunctionGaussian();
    void estimateInitialGuess(const FittingData& data, const FittingConfiguration& config);
    void computeResults();

};

#endif // FITTING_FUNCTION_GAUSSIAN_H
