#ifndef FITTING_FUNCTION_H
#define FITTING_FUNCTION_H

#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_multifit_nlin.h>
#include <gsl/gsl_sf_erf.h>
#include <gsl/gsl_sf_exp.h>
#include <gsl/gsl_math.h>

#include <string>
#include <map>
#include <iostream>

// #define SUPERSCRIPT_TWO "\xc2\xb2" // utf-8
#define SUPERSCRIPT_TWO "\xb2"  // latin-1

//debut TANGODEVIC-1295

namespace DataFitterVersion_ns
{
	///-----------------------------------------------------------------
   /// Free function: provide project version & name  
   ///-----------------------------------------------------------------
	const char* get_version();
	const char* get_name();
	
} //end namespace
 //fin TANGODEVIC-1295

////////////////////////
// Types Definitions ///
////////////////////////

struct FittingData {
    size_t n;
    gsl_vector* x;
    gsl_vector* y;
    gsl_vector* sigma;
};

struct FittingConfiguration {
    bool isReversed; // hint given
    int searchStoppingMethod;
    bool scaled;
    size_t maxIterations;
    double epsilon;
};

struct FittingResults {

    FittingResults(); // init results to 0 (TODO replace by nan)

    double qualityFactor;
    double sigma;
    double fwhm;
    double standardDeviation;
    double xLow; // sigmoid only
    double xHigh; // sigmoid only
    size_t iterations;
};

/// RAII class managing a gsl fit solver
class Solver {

public:

    Solver(bool scaled, size_t n, size_t p);
    ~Solver();

    gsl_multifit_fdfsolver* solver();

    void dumpState(size_t i);

private:
    gsl_multifit_fdfsolver* m_solver;

};

////////////
// Utils ///
////////////

/// returns -1 if a < b 0 if a == b or 1 (equivalent to signum(a-b))
template<typename T>
inline int sign_cmp(T a, T b) {
    return (b < a) - (a < b);
}

template<typename T>
inline T square(T x) {
    return x * x;
}

///                    |------ n -----|
/// fill a vector with [x0, x0+dx, ...] : such a mix of so called linspace & arange
inline void fillRange(gsl_vector* out, double x0, double dx, size_t n) {
    for (size_t i=0 ; i < n ; i++)
        gsl_vector_set(out, i, x0 + i*dx);
}

void dumpVector(const gsl_vector* data, size_t n);

/// estimate initial params from data : (position, width, height, background?)
/// NOTE used by FittingFunctionGaussian(Background) & FittingFunctionLorentzian(Background)
void gaussianEstimate(const FittingData& data, const FittingConfiguration& config, bool estimate_background, gsl_vector* out);

///debut TANGODEVIC-235
/// estimate initial params from data : (position, width, height, a, b)
///for background = a*x+b
///used by FittingFunctionGaussianWithFunctionBackground
/// and FittingFunctionLorentzianWithFunctionBackground
void gaussianEstimateWithFunctionBackground(const FittingData& data, const FittingConfiguration& config, bool estimate_background, gsl_vector* out);
///fin TANGODEVIC-235

/// Estimate initial params from data : (position, width, height, background?), based on sigmo�d shape
/// NOTE used by FittingFunctionSigmoid(Background) & FittingFunctionKnifeEdge(Background)
void sigmoidEstimate(const FittingData& data, const FittingConfiguration& config, bool estimate_background, gsl_vector* out);

typedef int (*PtrFunction) (const gsl_vector*, void *, gsl_vector *);
typedef int (*PtrFunctionDerivate) (const gsl_vector*, void *, gsl_matrix *);
typedef int (*PtrFunctionAndDerivate) (const gsl_vector*, void *, gsl_vector *, gsl_matrix *);

///////////////////////
// Fitting Function ///
///////////////////////

class FittingFunction {

public:
	
    // utils

    static double computeMean(gsl_vector* vVector);
    static double computeVariance(gsl_vector* vVector);
    static double computeResidual(gsl_vector* vVectorExp,gsl_vector* vVectorPred);
    static double computeDeterminationCoefficient(gsl_vector* vExperimentalDataY,gsl_vector* vFittedDataY);
    static double computeStandardDeviation(gsl_vector* vVector);

    // structors

    FittingFunction(size_t paramsCount);
    virtual ~FittingFunction();

    /// impl required

    virtual int generateFunctionFit(double dValMinX,double dResolutionX,long lNbPoints,gsl_vector* funcX,gsl_vector* funcY)=0;
    virtual int generateFunctionFit(long lNbPoints, gsl_vector *x, gsl_vector* funcX,gsl_vector* funcY)=0;
    virtual int generateDerivativeFunctionFit(double dValMinX,double dResolutionX,long lNbPoints,gsl_vector* funcX,gsl_vector* funcY)=0;
    virtual int generateDerivativeFunctionFit(long lNbPoints, gsl_vector *x, gsl_vector* funcX,gsl_vector* funcY)=0;

    inline const std::string& getEquation() const {return mEquation;}
    inline size_t getNbParameters() const {return mParamsCount;}

    // multifit

    virtual int doFit(FittingData& data, const FittingConfiguration& config, const gsl_vector *guess, bool estimate); /*!< default impl falls back to genericFit */

    int genericFit(FittingData& data, const FittingConfiguration& config, const gsl_vector *guess, bool estimate);
    void initializeInitialsParameters(const FittingData& data, const FittingConfiguration& config, const gsl_vector *guess, bool estimate);
    virtual void estimateInitialGuess(const FittingData& data, const FittingConfiguration& config) = 0;
    virtual void computeResults(); /*!< Fill fit results after genericFit finished (default does nothing) */

    // result accessors
    inline double getParameter(size_t i) const {return gsl_vector_get(mParameters, i);}
    inline double getParameterError(size_t i) const {return gsl_vector_get(mParametersErrors, i);}
    inline double getInitialGuess(size_t i) const {return gsl_vector_get(mInitialGuess, i);}
    inline double getXLow() const {return mFitResults.xLow;}
    inline double getXHigh() const {return mFitResults.xHigh;}
    inline double getQualityFactor() const {return mFitResults.qualityFactor;}
    inline double getSigma() const {return mFitResults.sigma;}
    inline double getFWHM() const {return mFitResults.fwhm;}
    inline double getHWHM() const {return .5 * mFitResults.fwhm;}
    inline size_t getNbIterations() const {return mFitResults.iterations;}
    inline double getStandardDeviation() const {return mFitResults.standardDeviation;}

    double computeObservedFStatistic(gsl_vector* vExperimentalDataY,gsl_vector* vFittedDataY);

protected :

    /// fit description
    std::string mName;
    std::string mEquation;
    size_t mParamsCount;
    /// fit results
    gsl_vector* mInitialGuess;
    gsl_vector* mParameters;
    gsl_vector* mParametersErrors;
    FittingResults mFitResults;
    /// callbacks for generic fit
    PtrFunction mFunction;
    PtrFunctionDerivate mFunctionDerivate;
    PtrFunctionAndDerivate mFunctionAndDerivate;
};

////////////////////////////////////
// Fitter based on Simple Functor //
////////////////////////////////////

template<typename T>
struct FunctionWrapper {

    /**
     * T requirements :
     * TODO protect functors against division by 0
     * * must be constructible with a const gsl_vector*
     * * callable with the following signature double(double)
     * * have a method derivate with the same signature
     * * have a method partial (TODO describe)
     */

    static const size_t size = T::size;

    static int f(const gsl_vector *params, void *user_data, gsl_vector *func) {
        const FittingData* data = (const FittingData*)user_data;
        T f(params);
        for (size_t i=0 ; i < data->n; i++) {
            double x = gsl_vector_get(data->x, i);
            double y = gsl_vector_get(data->y, i);
            double s = gsl_vector_get(data->sigma, i);
            gsl_vector_set(func, i, (f(x) - y) / s);
        }
        return GSL_SUCCESS;
    }

    static int df(const gsl_vector *params, void *user_data, gsl_matrix *jacobian) {
        const FittingData* data = (const FittingData*)user_data;
        T f(params);
        double dy[size];
        for (size_t i=0 ; i < data->n ; i++) {
            double x = gsl_vector_get(data->x, i);
            double s = gsl_vector_get(data->sigma, i);
            f.partial(x, dy);
            for(size_t j=0 ; j < size ; j++)
                gsl_matrix_set(jacobian, i, j, dy[j] / s);
        }
        return GSL_SUCCESS;
    }

    static int fdf(const gsl_vector *params, void *user_data ,gsl_vector *func, gsl_matrix *jacobian) {
        f(params, user_data, func);
        df(params, user_data, jacobian);
        return GSL_SUCCESS;
    }

    static int fill_f(const gsl_vector *params, double x0, double dx, size_t n, gsl_vector* fx, gsl_vector* fy) {
        T f(params);
        fillRange(fx, x0, dx, n);
        for (size_t i=0 ; i < n ; i++)
            gsl_vector_set(fy, i, f(gsl_vector_get(fx, i)));
        return 1;
    }

    static int fill_f(const gsl_vector *params, size_t n, const gsl_vector *x, gsl_vector* fx, gsl_vector* fy) {
        T f(params);
        for (size_t i=0 ; i < n ; i++) {
            double xi = gsl_vector_get(x,i);
            gsl_vector_set(fx, i, xi);
            gsl_vector_set(fy, i, f(xi));
        }
        return 1;
    }

    static int fill_df(const gsl_vector *params, double x0, double dx, size_t n, gsl_vector* fx, gsl_vector* fy) {
        T f(params);
        fillRange(fx, x0, dx, n);
        for (size_t i=0 ; i < n ; i++)
            gsl_vector_set(fy, i, f.derivate(gsl_vector_get(fx, i)));
        return 1;
    }

    static int fill_df(const gsl_vector *params, size_t n, const gsl_vector *x, gsl_vector* fx, gsl_vector* fy) {
        T f(params);
        for (size_t i=0 ; i < n ; i++) {
            double xi = gsl_vector_get(x,i);
            gsl_vector_set(fx, i, xi);
            gsl_vector_set(fy, i, f.derivate(xi));
        }
        return 1;
    }

};

template<typename T>
class GenericFitter : public FittingFunction {

public:

    typedef FunctionWrapper<T> Wrapper;

    GenericFitter() : FittingFunction(Wrapper::size) {
        mFunction = Wrapper::f;
        mFunctionDerivate = Wrapper::df;
        mFunctionAndDerivate = Wrapper::fdf;
    }

    int generateFunctionFit(double dValMinX,double dResolutionX,long lNbPoints,gsl_vector* funcX,gsl_vector* funcY) {
        return Wrapper::fill_f(mParameters, dValMinX, dResolutionX, lNbPoints, funcX, funcY);
    }

    int generateFunctionFit(long lNbPoints, gsl_vector *x, gsl_vector* funcX,gsl_vector* funcY) {
        return Wrapper::fill_f(mParameters, lNbPoints, x, funcX, funcY);
    }

    int generateDerivativeFunctionFit(double dValMinX,double dResolutionX,long lNbPoints,gsl_vector* funcX,gsl_vector* funcY) {
        return Wrapper::fill_df(mParameters, dValMinX, dResolutionX, lNbPoints, funcX, funcY);
    }

    int generateDerivativeFunctionFit(long lNbPoints, gsl_vector *x, gsl_vector* funcX,gsl_vector* funcY) {
        return Wrapper::fill_df(mParameters, lNbPoints, x, funcX, funcY);
    }

};

#endif // FITTING_FUNCTION_H
