#ifndef FITTING_FUNCTION_FACTORY_H
#define FITTING_FUNCTION_FACTORY_H

#include <set>
#include <string>
#include "FittingFunction.h"


class FittingFunctionFactory {

public:

    // get the list of all known filter by name
    std::set<std::string> knownFitters();

    /// builds a new Fitting Function from its name
    /// return NULL if name is not a known filter
    FittingFunction* newFitter(const std::string& name);

};

#endif // FITTING_FUNCTION_FACTORY_H
