#ifndef FITTING_FUNCTION_LORENTZIANB_H
#define FITTING_FUNCTION_LORENTZIANB_H

#include "FittingFunction.h"

struct LorentzianbFunction {

    static const size_t size = 4;

    double pos, width, height, background;

    LorentzianbFunction(const gsl_vector* params) {
        pos = gsl_vector_get(params, 0);
        width = gsl_vector_get(params, 1);
        height = gsl_vector_get(params, 2);
        background = gsl_vector_get(params, 3);
    }

    inline double operator()(double x) const {
        return height / (1. + square((x-pos)/width)) + background;
    }

    inline double derivate(double x) const {
        double rcx = (x-pos)/width;
        double e = 1. + square(rcx);
        return - 2. * height / width * rcx / square(e);
    }

    inline void partial(double x, double* dy) const {
        double rcx = (x-pos)/width;
        double e = 1. + square(rcx);
        dy[0] = 2. * height / width * rcx / square(e);
        dy[1] = 2. * height / width * square(rcx) / square(e);
        dy[2] = 1./e;
        dy[3] = 1.;
    }

};

class FittingFunctionLorentzianWithBackground : public GenericFitter<LorentzianbFunction> {

public:

    FittingFunctionLorentzianWithBackground();
    void estimateInitialGuess(const FittingData& data, const FittingConfiguration& config);
    void computeResults();

};

#endif // FITTING_FUNCTION_LORENTZIANB_H
