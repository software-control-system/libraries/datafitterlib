#ifndef FITTING_FUNCTION_SIGMOID_H
#define FITTING_FUNCTION_SIGMOID_H

#include "FittingFunction.h"

struct SigmoidFunction {

    static const size_t size = 3;

    double pos, width, height;

    SigmoidFunction(const gsl_vector* params) {
        pos = gsl_vector_get(params, 0);
        width = gsl_vector_get(params, 1);
        height = gsl_vector_get(params, 2);
    }

    inline double operator()(double x) const {
        return height / (1. + exp(-(x-pos)/width));
    }

    inline double derivate(double x) const {
        double rcx = (x-pos)/width;
        double expo = exp(-rcx);
        return  height/width * expo / square(1. + expo);
    }

    inline void partial(double x, double* dy) const {
        double rcx = (x-pos)/width;
        double expo = exp(-rcx);
        double e = 1. + expo;
        double e2 = square(e);
        dy[0] = - height/width * expo / e2;
        dy[1] = - height/width * rcx * expo / e2;
        dy[2] = 1. / e;
    }

};

class FittingFunctionSigmoid : public GenericFitter<SigmoidFunction> {

public:

    FittingFunctionSigmoid();
    void estimateInitialGuess(const FittingData& data, const FittingConfiguration& config);
    void computeResults();

};

#endif // FITTING_FUNCTION_SIGMOID_H
