#ifndef FITTING_FUNCTION_LORENTZIAN_H
#define FITTING_FUNCTION_LORENTZIAN_H

#include "FittingFunction.h"

struct LorentzianFunction {

    static const size_t size = 3;

    double pos, width, height;

    LorentzianFunction(const gsl_vector* params) {
        pos = gsl_vector_get(params, 0);
        width = gsl_vector_get(params, 1);
        height = gsl_vector_get(params, 2);
    }

    inline double operator()(double x) const {
        return height / (1. + square((x-pos)/width));
    }

    inline double derivate(double x) const {
        double rcx = (x-pos)/width;
        double e = 1. + square(rcx);
        return - 2. * height / width * rcx / square(e);
    }

    inline void partial(double x, double* dy) const {
        double rcx = (x-pos)/width;
        double e = 1. + square(rcx);
        dy[0] = 2. * height / width * rcx / square(e);
        dy[1] = 2. * height / width * square(rcx) / square(e);
        dy[2] = 1./e;
    }

};

class FittingFunctionLorentzian : public GenericFitter<LorentzianFunction> {

public:

    FittingFunctionLorentzian();
    void estimateInitialGuess(const FittingData& data, const FittingConfiguration& config);
    void computeResults();


};

#endif // FITTING_FUNCTION_LORENTZIAN_H
