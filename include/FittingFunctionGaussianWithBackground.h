#ifndef FITTING_FUNCTION_GAUSSIANB_H
#define FITTING_FUNCTION_GAUSSIANB_H

#include "FittingFunction.h"

struct GaussianbFunction {

    static const size_t size = 4;

    double pos, width, height, background;

    GaussianbFunction(const gsl_vector* params) {
        pos = gsl_vector_get(params, 0);
        width = gsl_vector_get(params, 1);
        height = gsl_vector_get(params, 2);
        background = gsl_vector_get(params, 3);
    }

    inline double operator()(double x) const {
        return height * exp(-.5 * square((x-pos)/width)) + background;
    }

    inline double derivate(double x) const {
        double rcx = (x-pos)/width;
        return - height * rcx / width * exp(-.5 * square(rcx));
    }

    inline void partial(double x, double* dy) const {
        double rcx = (x-pos)/width;
        double rcx2 = square(rcx);
        double e = exp(-.5 * rcx2);
        dy[0] = height * rcx/width * e;
        dy[1] = height * rcx2/width * e;
        dy[2] = e;
        dy[3] = 1.;
    }

};

class FittingFunctionGaussianWithBackground : public GenericFitter<GaussianbFunction> {

public:

    FittingFunctionGaussianWithBackground();
    void estimateInitialGuess(const FittingData& data, const FittingConfiguration& config);
    void computeResults();

};

#endif // FITTING_FUNCTION_GAUSSIANB_H
