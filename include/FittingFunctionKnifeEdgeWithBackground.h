#ifndef FITTING_FUNCTION_KNIFE_EDGEB_H
#define FITTING_FUNCTION_KNIFE_EDGEB_H

#include "FittingFunction.h"

struct KnifeEdgebFunction {

	static const size_t size = 4;

	double mu, sigma, height, background;

	KnifeEdgebFunction(const gsl_vector* params) {
		mu = gsl_vector_get(params, 0);         // Mu is associated to "Position"
		sigma = gsl_vector_get(params, 1);      // Sigma is associated to "Width"
		height = gsl_vector_get(params, 2);
		background = gsl_vector_get(params, 3);
	}

	// Function calculation.
	inline double operator()(double x) const {
		// Test to avoid a division by zero
		if (sigma == 0.0) return GSL_NAN;
		double XX = ((x - mu) * M_SQRT1_2) / sigma;
		return 0.5 * height * (1.0 + gsl_sf_erf(XX)) + background;
	}

	// Derivative function calculation.
	inline double derivate(double x) const {
		// Test to avoid a division by zero
		if (sigma == 0.0) return GSL_NAN;
		double tmp = -0.5 * square((x - mu) / sigma);

		if (tmp < -100.0)
		{
			return (height * 0. * M_SQRT1_2) / (M_SQRTPI * sigma);
		}
		else
		{
			return (height * gsl_sf_exp(tmp) * M_SQRT1_2) / (M_SQRTPI * sigma);
		}
		
	}

	// Partial derivatives calculations for each parameter to optimize.
	inline void partial(double x, double* dy) const {
		// Test to avoid a division by zero
		if (sigma == 0.0)
		{
			dy[0] = GSL_NAN;
			dy[1] = GSL_NAN;
			dy[2] = GSL_NAN;
			dy[3] = GSL_NAN;
			return;
		}
		// Common variables (to minimize the amount of operations):
		double tmp_delta = ((x - mu) * M_SQRT1_2) / sigma;
		double tmp_delta_squared = -1.0 * square(tmp_delta);
		if(tmp_delta_squared < -100.0)
		{
			dy[0] = (-1.0 * height * 0. * M_SQRT1_2) / (M_SQRTPI * sigma); // Partial for mu
		}
		else
		{
			dy[0] = (-1.0 * height * gsl_sf_exp(tmp_delta_squared) * M_SQRT1_2) / (M_SQRTPI * sigma); // Partial for mu
		}
		dy[1] = dy[0] * (x - mu) / sigma; // Partial for sigma
		dy[2] = 0.5 + 0.5 * gsl_sf_erf(tmp_delta); // Partial for height
		dy[3] = 1.; // Partial for background
	}

};

class FittingFunctionKnifeEdgeWithBackground : public GenericFitter<KnifeEdgebFunction> {

public:

	FittingFunctionKnifeEdgeWithBackground();
	void estimateInitialGuess(const FittingData& data, const FittingConfiguration& config);
	void computeResults();

};

#endif // FITTING_FUNCTION_KNIFE_EDGEB_H

